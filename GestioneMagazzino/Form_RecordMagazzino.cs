﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GestioneMagazzino
{
    public partial class Form_RecordMagazzino : Form
    {
        private int indexRecordToEdit = -1;
        private Utente user;

        public Form_RecordMagazzino()
        {
            try
            {
                InitializeComponent();
                DataTable dt = Utility.getDataTableFromDB("SELECT DISTINCT [Model] FROM [archivi].[dbo].[VISTA_SM_FC_2]");
                cmb_modelloCollimatore.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    cmb_modelloCollimatore.Items.Add(row["Model"].ToString());
                }
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_RecordMagazzino(Utente user) : this()
        {
            try
            {
                this.user = user;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Overload del costruttore con 0 parametri, richiamato tramite :this()
        /// </summary>
        /// <param name="indexRecordToEdit"></param>
        public Form_RecordMagazzino(Utente user,int indexRecordToEdit, bool isCloned) : this()
        {
            try
            {
                this.user = user;
                this.indexRecordToEdit = indexRecordToEdit;
                fillForm();
                if (isCloned)
                    this.indexRecordToEdit = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Tb_SerialeCollimatore_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tb_SerialeCollimatore.MaskFull)
                {
                    DataTable dt = Utility.getDataTableFromDB("SELECT TOP (1) [idRMA],[serialeColl],[modelloColl],[numFlowChart] FROM [RMA_Collimatore] WHERE [serialeColl] = '" + tb_SerialeCollimatore.Text + "' ORDER BY [idProgressivo] DESC");
                    if (!dt.Rows.Count.Equals(0))
                    {
                        cmb_modelloCollimatore.SelectedItem = dt.Rows[0].Field<string>("modelloColl");
                        tb_flowChart.Text = dt.Rows[0].Field<string>("numFlowChart");
                        tb_CNC.Text = dt.Rows[0].Field<string>("idRMA");
                    }
                }
                else
                {
                    cmb_modelloCollimatore.SelectedIndex = -1;
                    tb_flowChart.Text = "";
                    tb_CNC.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillForm()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [gestioneMagazzino] WHERE [ID] = '" + indexRecordToEdit.ToString() + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    tb_SerialeCollimatore.Text = dt.Rows[0].Field<int>("Seriale Collimatore").ToString();
                    cmb_modelloCollimatore.SelectedItem = dt.Rows[0].Field<string>("Modello Collimatore");
                    tb_flowChart.Text = dt.Rows[0].Field<string>("Flow Chart");
                    tb_CNC.Text = dt.Rows[0].Field<string>("CNC");
                    dtp_dataArrivo.Text = dt.Rows[0].Field<DateTime>("Data Arrivo").ToShortDateString();
                    tb_scaffale.Text = dt.Rows[0].Field<string>("Scaffale");
                    cmb_statoRiparazione.SelectedItem = dt.Rows[0].Field<string>("Stato");
                    tb_noteRiparazione.Text = dt.Rows[0].Field<string>("Note");
                }
                else
                {
                    dtp_dataArrivo.Text = DateTime.Now.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void brn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (indexRecordToEdit.Equals(-1))
                    searchRecordInDB();
                else
                    updateRecordInDB();
                if (this.DialogResult.Equals(DialogResult.OK))
                    this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchRecordInDB()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [gestioneMagazzino]  WHERE " +
                    "[Seriale Collimatore] = '" + tb_SerialeCollimatore.Text + "' AND " +
                    "[Modello Collimatore] = '" + (cmb_modelloCollimatore.SelectedItem == null ? "" : cmb_modelloCollimatore.SelectedItem.ToString()) + "' AND " +
                    "[Flow Chart] = '" + tb_flowChart.Text + "' AND " +
                    "CNC = '" + tb_CNC.Text + "' AND " +
                    "Scaffale = '" + tb_scaffale.Text + "' AND " +
                    "Stato = '" + (cmb_statoRiparazione.SelectedItem == null ? "" : cmb_statoRiparazione.SelectedItem.ToString()) + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    DialogResult result = MessageBox.Show("Il record già presente nel Database verrà aggiornato con i dati immessi nel form.\nSei sicuro di voler procedere?", "Aggiornamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result.Equals(DialogResult.Yes))
                    {
                        indexRecordToEdit = dt.Rows[0].Field<int>(0);
                        updateRecordInDB();
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                        this.DialogResult = DialogResult.Abort;
                }
                else
                    insertRecordInDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void insertRecordInDB()
        {
            try
            {
                Utility.executeQuery("INSERT INTO [gestioneMagazzino] " +
                "([Seriale Collimatore],[Modello Collimatore],[Flow Chart],CNC,[Data Arrivo],Scaffale,Stato,[Data Aggiornamento],[Aggiornato Da],Note) " +
                "VALUES ('" + (tb_SerialeCollimatore.Text == "" ? "0" : tb_SerialeCollimatore.Text) + "'," +
                    "'" + (cmb_modelloCollimatore.SelectedItem == null ? "" : cmb_modelloCollimatore.SelectedItem.ToString()) + "'," +
                    "'" + tb_flowChart.Text + "'," +
                    "'" + tb_CNC.Text + "'," +
                    "'" + dtp_dataArrivo.Value.ToShortDateString() + "'," +
                    "'" + tb_scaffale.Text + "'," +
                    "'" + (cmb_statoRiparazione.SelectedItem == null ? "" : cmb_statoRiparazione.SelectedItem.ToString()) + "'," +
                    "'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + user.Nome + " " + user.Cognome + "'," +
                    "'" + tb_noteRiparazione.Text + "')");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateRecordInDB()
        {
            try
            {
                Utility.executeQuery("UPDATE [gestioneMagazzino] SET " +
                "[Seriale Collimatore] = '" + (tb_SerialeCollimatore.Text == "" ? "0" : tb_SerialeCollimatore.Text) + "'," +
                "[Modello Collimatore] = '" + (cmb_modelloCollimatore.SelectedItem == null ? "" : cmb_modelloCollimatore.SelectedItem.ToString()) + "'," +
                "[Flow Chart] = '" + tb_flowChart.Text + "'," +
                "CNC = '" + tb_CNC.Text + "'," +
                "[Data Arrivo] = '" + dtp_dataArrivo.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Scaffale] = '" + tb_scaffale.Text + "'," +
                "Stato = '" + (cmb_statoRiparazione.SelectedItem == null ? "" : cmb_statoRiparazione.SelectedItem.ToString()) + "'," +
                "[Data Aggiornamento] = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Aggiornato Da] = '" + user.Nome + " " + user.Cognome + "'," +
                "[Note] = '" + tb_noteRiparazione.Text + "'" +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
