﻿namespace GestioneMagazzino
{
    partial class Form_Main
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lbl_NomeUtente = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddMagazzino = new System.Windows.Forms.ToolStripButton();
            this.btn_CloneMagazzino = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_ColorLines = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_RemoveMagazzino = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchMagazzino = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_ExportMagazzino = new System.Windows.Forms.ToolStripButton();
            this.btn_PrintTestFiltro = new System.Windows.Forms.ToolStripButton();
            this.dgv_gestioneMagazzino = new System.Windows.Forms.DataGridView();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_gestioneMagazzino)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.lbl_NomeUtente,
            this.toolStripSeparator1,
            this.btn_AddMagazzino,
            this.btn_CloneMagazzino,
            this.toolStripSeparator10,
            this.btn_ColorLines,
            this.toolStripSeparator3,
            this.btn_RemoveMagazzino,
            this.toolStripSeparator11,
            this.tstb_SearchMagazzino,
            this.toolStripSeparator12,
            this.btn_ExportMagazzino,
            this.btn_PrintTestFiltro});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1242, 50);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 50);
            // 
            // lbl_NomeUtente
            // 
            this.lbl_NomeUtente.Name = "lbl_NomeUtente";
            this.lbl_NomeUtente.Size = new System.Drawing.Size(78, 47);
            this.lbl_NomeUtente.Text = "Nome Utente";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddMagazzino
            // 
            this.btn_AddMagazzino.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_AddMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddMagazzino.Image")));
            this.btn_AddMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddMagazzino.Name = "btn_AddMagazzino";
            this.btn_AddMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_AddMagazzino.ToolTipText = "Aggiungi A Magazzino";
            this.btn_AddMagazzino.Click += new System.EventHandler(this.btn_AddMagazzino_Click);
            // 
            // btn_CloneMagazzino
            // 
            this.btn_CloneMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CloneMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_CloneMagazzino.Image")));
            this.btn_CloneMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CloneMagazzino.Name = "btn_CloneMagazzino";
            this.btn_CloneMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_CloneMagazzino.Text = "Clona Record";
            this.btn_CloneMagazzino.Click += new System.EventHandler(this.btn_CloneMagazzino_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator10.Visible = false;
            // 
            // btn_ColorLines
            // 
            this.btn_ColorLines.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ColorLines.Image = ((System.Drawing.Image)(resources.GetObject("btn_ColorLines.Image")));
            this.btn_ColorLines.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ColorLines.Name = "btn_ColorLines";
            this.btn_ColorLines.Size = new System.Drawing.Size(51, 47);
            this.btn_ColorLines.Text = "Evidenzia";
            this.btn_ColorLines.ToolTipText = "Evidenzia";
            this.btn_ColorLines.Visible = false;
            this.btn_ColorLines.Click += new System.EventHandler(this.btn_ColorLines_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator3.Visible = false;
            // 
            // btn_RemoveMagazzino
            // 
            this.btn_RemoveMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RemoveMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_RemoveMagazzino.Image")));
            this.btn_RemoveMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RemoveMagazzino.Name = "btn_RemoveMagazzino";
            this.btn_RemoveMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_RemoveMagazzino.Text = "toolStripButton2";
            this.btn_RemoveMagazzino.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_RemoveMagazzino.Visible = false;
            this.btn_RemoveMagazzino.Click += new System.EventHandler(this.btn_RemoveMagazzino_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchMagazzino
            // 
            this.tstb_SearchMagazzino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchMagazzino.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchMagazzino.Name = "tstb_SearchMagazzino";
            this.tstb_SearchMagazzino.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchMagazzino.Text = "Cerca...";
            this.tstb_SearchMagazzino.ToolTipText = "Cerca In Magazzino";
            this.tstb_SearchMagazzino.Leave += new System.EventHandler(this.tstb_SearchMagazzino_Leave);
            this.tstb_SearchMagazzino.Click += new System.EventHandler(this.tstb_SearchMagazzino_Click);
            this.tstb_SearchMagazzino.TextChanged += new System.EventHandler(this.tstb_SearchMagazzino_TextChanged);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_ExportMagazzino
            // 
            this.btn_ExportMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ExportMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_ExportMagazzino.Image")));
            this.btn_ExportMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ExportMagazzino.Name = "btn_ExportMagazzino";
            this.btn_ExportMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_ExportMagazzino.Text = "toolStripButton4";
            this.btn_ExportMagazzino.ToolTipText = "Esporta Magazzino";
            this.btn_ExportMagazzino.Click += new System.EventHandler(this.btn_ExportMagazzino_Click);
            // 
            // btn_PrintTestFiltro
            // 
            this.btn_PrintTestFiltro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_PrintTestFiltro.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintTestFiltro.Image")));
            this.btn_PrintTestFiltro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_PrintTestFiltro.Name = "btn_PrintTestFiltro";
            this.btn_PrintTestFiltro.Size = new System.Drawing.Size(51, 47);
            this.btn_PrintTestFiltro.ToolTipText = "Stampa Test";
            this.btn_PrintTestFiltro.Visible = false;
            this.btn_PrintTestFiltro.Click += new System.EventHandler(this.btn_PrintTestFiltro_Click);
            // 
            // dgv_gestioneMagazzino
            // 
            this.dgv_gestioneMagazzino.AllowUserToAddRows = false;
            this.dgv_gestioneMagazzino.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_gestioneMagazzino.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_gestioneMagazzino.Location = new System.Drawing.Point(0, 50);
            this.dgv_gestioneMagazzino.Name = "dgv_gestioneMagazzino";
            this.dgv_gestioneMagazzino.ReadOnly = true;
            this.dgv_gestioneMagazzino.Size = new System.Drawing.Size(1242, 462);
            this.dgv_gestioneMagazzino.TabIndex = 7;
            this.dgv_gestioneMagazzino.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_gestioneMagazzino_CellClick);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 512);
            this.Controls.Add(this.dgv_gestioneMagazzino);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.Text = "Gestione Magazzino";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Main_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_gestioneMagazzino)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_AddMagazzino;
        private System.Windows.Forms.ToolStripButton btn_RemoveMagazzino;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchMagazzino;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton btn_ExportMagazzino;
        private System.Windows.Forms.ToolStripButton btn_PrintTestFiltro;
        private System.Windows.Forms.DataGridView dgv_gestioneMagazzino;
        private System.Windows.Forms.ToolStripLabel lbl_NomeUtente;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btn_CloneMagazzino;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton btn_ColorLines;
    }
}

