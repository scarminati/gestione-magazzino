﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Data.SqlClient;
using System.IO;

namespace GestioneMagazzino
{
    public partial class Form_Main : Form
    {
        private string selectCondition = "";
        private Utente user;
        private bool changeColorLines = false;
        private Color colorLines = Color.White;
        public Form_Main(Utente user)
        {
            try
            {
                InitializeComponent();
                setApplicationVersion();
                this.user = user;
                lbl_NomeUtente.Text = user.Nome + " " + user.Cognome;
                dgv_gestioneMagazzino.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [gestioneMagazzino]");
                setDataGridViewLayout();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region MetodiGenerali

        private void setApplicationVersion()
        {
            // considero solo i primi 2 valori per la versione da visualizzare
            String tmp = Application.ProductVersion;
            int pos = tmp.IndexOf(".");
            pos = tmp.IndexOf(".", pos + 1);

            this.Text += " - V. " + tmp.Substring(0, pos);
        }

        /// <summary>
        /// Ripristina il valore di default della textbox di ricerca e tutte le datagridview
        /// </summary>
        /// <param name="tb"></param>
        private void resetSearchTextboxAndDatagridviews(ToolStripTextBox tb)
        {
            if (tstb_SearchMagazzino.Text.Equals(""))
            {
                tstb_SearchMagazzino.Text = "Cerca...";
                dgv_gestioneMagazzino.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [gestioneMagazzino]");
                setDataGridViewLayout();
            }
            //aggiungere le atre ricerche QUI...
        }

        /// <summary>
        /// modifica la visualizzazione dei dati nella datagridview in accordo con la selezione scritta nella corrispondente textbox di ricerca
        /// </summary>
        /// <param name="tb"></param>
        private void changeDataDisplayed(ToolStripTextBox tb)
        {
            try
            {
                if (!tstb_SearchMagazzino.Text.Equals("") && !tstb_SearchMagazzino.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("gestioneMagazzino", tstb_SearchMagazzino.Text);
                    dgv_gestioneMagazzino.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [gestioneMagazzino] " + selectCondition);
                    setDataGridViewLayout();
                }
                //aggiungere le atre ricerche QUI...
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setDataGridViewLayout()
        {
            try
            {
                dgv_gestioneMagazzino.Columns["ID"].Visible = false;
                if (changeColorLines)
                {
                    foreach (DataGridViewRow r in dgv_gestioneMagazzino.Rows)
                        if (r.Cells["Esito Test"].Value.ToString().Equals("Fail"))
                            r.DefaultCellStyle.BackColor = colorLines;
                    dgv_gestioneMagazzino.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Gestione Magazzino

        private void dgv_gestioneMagazzino_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex!= -1 && dgv_gestioneMagazzino.Rows[dgv_gestioneMagazzino.CurrentCell.RowIndex].Cells["ID"].Value.ToString() != "" && !dgv_gestioneMagazzino.SelectedCells.Count.Equals(dgv_gestioneMagazzino.Rows[dgv_gestioneMagazzino.CurrentCell.RowIndex].Cells.Count))
                {
                    int indexRecordToEdit = Convert.ToInt32(dgv_gestioneMagazzino.Rows[dgv_gestioneMagazzino.CurrentCell.RowIndex].Cells["ID"].Value.ToString());
                    Form_RecordMagazzino formArt = new Form_RecordMagazzino(user,indexRecordToEdit, false);
                    var dialogResult = formArt.ShowDialog();
                    if (dialogResult.Equals(DialogResult.OK))
                        dgv_gestioneMagazzino.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [gestioneMagazzino]");
                    setDataGridViewLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tstb_SearchMagazzino_Click(object sender, EventArgs e)
        {
            tstb_SearchMagazzino.Text = "";
            tstb_SearchMagazzino_TextChanged(null, null);
        }

        private void tstb_SearchMagazzino_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchMagazzino);
        }

        private void tstb_SearchMagazzino_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchMagazzino);
        }

        private void btn_AddMagazzino_Click(object sender, EventArgs e)
        {
            try
            {
                Form_RecordMagazzino formRip = new Form_RecordMagazzino(user);
                var dialogResult = formRip.ShowDialog();
                if (dialogResult.Equals(DialogResult.OK))
                    dgv_gestioneMagazzino.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [gestioneMagazzino]");
                setDataGridViewLayout();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CloneMagazzino_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv_gestioneMagazzino.Rows[dgv_gestioneMagazzino.CurrentCell.RowIndex].Cells["ID"].Value.ToString() != "")
                {
                    int indexRecordToEdit = Convert.ToInt32(dgv_gestioneMagazzino.Rows[dgv_gestioneMagazzino.CurrentCell.RowIndex].Cells["ID"].Value.ToString());
                    Form_RecordMagazzino formArt = new Form_RecordMagazzino(user, indexRecordToEdit, true);
                    var dialogResult = formArt.ShowDialog();
                    if (dialogResult.Equals(DialogResult.OK))
                        dgv_gestioneMagazzino.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [gestioneMagazzino]");
                    setDataGridViewLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ColorLines_Click(object sender, EventArgs e)
        {
            try
            {
                changeColorLines = true;
                if (colorLines.Equals(Color.White))
                    colorLines = Color.Salmon;
                else
                    colorLines = Color.White;
                setDataGridViewLayout();
                changeColorLines = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RemoveMagazzino_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("L'elemento selezionato verrà rimosso dal database.\nSei sicuro di voler procedere?", "Elimina Riparazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    int actualIndex = dgv_gestioneMagazzino.CurrentCell.RowIndex;
                    Utility.executeQuery("DELETE FROM [gestioneMagazzino] WHERE " +
                           "[Id] = '" + dgv_gestioneMagazzino.Rows[actualIndex].Cells["Id"].Value.ToString() + "'");
                    tstb_SearchMagazzino.Text = "";
                    dgv_gestioneMagazzino.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [gestioneMagazzino] ");
                    setDataGridViewLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ExportMagazzino_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.saveAndVisualizeExcelReport(dgv_gestioneMagazzino, "Report Magazzino");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_PrintTestFiltro_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.saveAndVisualizeWordReport(dgv_gestioneMagazzino, dgv_gestioneMagazzino.CurrentCell.RowIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void Form_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
