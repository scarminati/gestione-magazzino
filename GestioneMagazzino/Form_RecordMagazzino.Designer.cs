﻿namespace GestioneMagazzino
{
    partial class Form_RecordMagazzino
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_RecordMagazzino));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtp_dataArrivo = new System.Windows.Forms.DateTimePicker();
            this.tb_scaffale = new System.Windows.Forms.TextBox();
            this.cmb_modelloCollimatore = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_CNC = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_flowChart = new System.Windows.Forms.MaskedTextBox();
            this.tb_SerialeCollimatore = new System.Windows.Forms.MaskedTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmb_statoRiparazione = new System.Windows.Forms.ComboBox();
            this.tb_noteRiparazione = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.brn_OK = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Modello";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(236, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Scaffale";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Arrivo Collimatore";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Seriale";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(333, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Flow Chart";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Note Riparazione";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtp_dataArrivo);
            this.groupBox1.Controls.Add(this.tb_scaffale);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(483, 96);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Magazzino";
            // 
            // dtp_dataArrivo
            // 
            this.dtp_dataArrivo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_dataArrivo.Location = new System.Drawing.Point(15, 55);
            this.dtp_dataArrivo.Name = "dtp_dataArrivo";
            this.dtp_dataArrivo.Size = new System.Drawing.Size(121, 20);
            this.dtp_dataArrivo.TabIndex = 5;
            // 
            // tb_scaffale
            // 
            this.tb_scaffale.Location = new System.Drawing.Point(200, 55);
            this.tb_scaffale.Name = "tb_scaffale";
            this.tb_scaffale.Size = new System.Drawing.Size(121, 20);
            this.tb_scaffale.TabIndex = 6;
            // 
            // cmb_modelloCollimatore
            // 
            this.cmb_modelloCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_modelloCollimatore.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_modelloCollimatore.BackColor = System.Drawing.SystemColors.Window;
            this.cmb_modelloCollimatore.FormattingEnabled = true;
            this.cmb_modelloCollimatore.Items.AddRange(new object[] {
            "Argento",
            "Molibdeno"});
            this.cmb_modelloCollimatore.Location = new System.Drawing.Point(82, 47);
            this.cmb_modelloCollimatore.Name = "cmb_modelloCollimatore";
            this.cmb_modelloCollimatore.Size = new System.Drawing.Size(239, 21);
            this.cmb_modelloCollimatore.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_CNC);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tb_flowChart);
            this.groupBox2.Controls.Add(this.tb_SerialeCollimatore);
            this.groupBox2.Controls.Add(this.cmb_modelloCollimatore);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(483, 79);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Collimatore";
            // 
            // tb_CNC
            // 
            this.tb_CNC.Location = new System.Drawing.Point(406, 48);
            this.tb_CNC.Mask = "0000/00";
            this.tb_CNC.Name = "tb_CNC";
            this.tb_CNC.Size = new System.Drawing.Size(64, 20);
            this.tb_CNC.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(422, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "CNC";
            // 
            // tb_flowChart
            // 
            this.tb_flowChart.Location = new System.Drawing.Point(331, 48);
            this.tb_flowChart.Mask = "0000/0000";
            this.tb_flowChart.Name = "tb_flowChart";
            this.tb_flowChart.Size = new System.Drawing.Size(64, 20);
            this.tb_flowChart.TabIndex = 3;
            // 
            // tb_SerialeCollimatore
            // 
            this.tb_SerialeCollimatore.Location = new System.Drawing.Point(15, 48);
            this.tb_SerialeCollimatore.Mask = "0000000";
            this.tb_SerialeCollimatore.Name = "tb_SerialeCollimatore";
            this.tb_SerialeCollimatore.Size = new System.Drawing.Size(57, 20);
            this.tb_SerialeCollimatore.TabIndex = 1;
            this.tb_SerialeCollimatore.TextChanged += new System.EventHandler(this.Tb_SerialeCollimatore_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmb_statoRiparazione);
            this.groupBox3.Controls.Add(this.tb_noteRiparazione);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(12, 199);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(483, 305);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Riparazione";
            // 
            // cmb_statoRiparazione
            // 
            this.cmb_statoRiparazione.FormattingEnabled = true;
            this.cmb_statoRiparazione.Items.AddRange(new object[] {
            "Accettazione",
            "Registrato",
            "Analisi",
            "In Attesa Di Preventivo",
            "In Riparazione",
            "Riparato",
            "Da Insacchettare",
            "Fine Lavorazione",
            "Spedito"});
            this.cmb_statoRiparazione.Location = new System.Drawing.Point(21, 47);
            this.cmb_statoRiparazione.Name = "cmb_statoRiparazione";
            this.cmb_statoRiparazione.Size = new System.Drawing.Size(121, 21);
            this.cmb_statoRiparazione.TabIndex = 7;
            // 
            // tb_noteRiparazione
            // 
            this.tb_noteRiparazione.Location = new System.Drawing.Point(21, 107);
            this.tb_noteRiparazione.Multiline = true;
            this.tb_noteRiparazione.Name = "tb_noteRiparazione";
            this.tb_noteRiparazione.Size = new System.Drawing.Size(447, 185);
            this.tb_noteRiparazione.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(35, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Stato Riparazione";
            // 
            // brn_OK
            // 
            this.brn_OK.ImageKey = "ok.png";
            this.brn_OK.ImageList = this.imageList;
            this.brn_OK.Location = new System.Drawing.Point(212, 526);
            this.brn_OK.Name = "brn_OK";
            this.brn_OK.Size = new System.Drawing.Size(50, 50);
            this.brn_OK.TabIndex = 20;
            this.brn_OK.UseVisualStyleBackColor = true;
            this.brn_OK.Click += new System.EventHandler(this.brn_OK_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "save.png");
            this.imageList.Images.SetKeyName(1, "export.png");
            this.imageList.Images.SetKeyName(2, "ok.png");
            // 
            // Form_RecordMagazzino
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 595);
            this.Controls.Add(this.brn_OK);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_RecordMagazzino";
            this.Text = "Inserisci In Magazzino";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtp_dataArrivo;
        private System.Windows.Forms.TextBox tb_scaffale;
        private System.Windows.Forms.ComboBox cmb_modelloCollimatore;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox tb_flowChart;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmb_statoRiparazione;
        private System.Windows.Forms.TextBox tb_noteRiparazione;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button brn_OK;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.MaskedTextBox tb_SerialeCollimatore;
        private System.Windows.Forms.MaskedTextBox tb_CNC;
        private System.Windows.Forms.Label label7;
    }
}

